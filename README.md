## Hidden Markov Models for Protein Detection

### Document Info
Start Date: 2020 AUG 11 <br/>
Creator ID: CNB <br/>
Last Edit Date: 2020 AUG 12

### Program Dependencies
* python; `which python`
* prokka; `which prokka`
* prodigal;
* blast; 
* LithoGenie / FeGenie; 
* [genbank_to_fasta](http://rocaplab.ocean.washington.edu/files/genbank_to_fasta_v1.1.zip); record date of accession
* clustalo; `which clustalo`
* HMMER; `which hmmbuild`, `which hmmsearch`

```Seemann, T. (2014). Prokka: rapid prokaryotic genome annotation. Bioinformatics, 30(14), 2068-2069.```

```Hyatt, D., Chen, G. L., LoCascio, P. F., Land, M. L., Larimer, F. W., & Hauser, L. J. (2010). Prodigal: prokaryotic gene recognition and translation initiation site identification. BMC bioinformatics, 11(1), 119.```

```Madden, T. (2013). The BLAST sequence analysis tool. In The NCBI Handbook [Internet]. 2nd edition. National Center for Biotechnology Information (US).```

```Garber, A. I., Nealson, K. H., Okamoto, A., McAllister, S. M., Chan, C. S., Barco, R. A., & Merino, N. (2020). FeGenie: A comprehensive tool for the identification of iron genes and iron gene neighborhoods in genome and metagenome assemblies. Frontiers in microbiology, 11, 37.```

```Sievers, F., Wilm, A., Dineen, D., Gibson, T. J., Karplus, K., Li, W., ... & Thompson, J. D. (2011). Fast, scalable generation of high‐quality protein multiple sequence alignments using Clustal Omega. Molecular systems biology, 7(1), 539.```

```Eddy, S. R. (1995, July). Multiple alignment using hidden Markov models. In Ismb (Vol. 3, pp. 114-120).```




### Annotating the Metagenome Assembly (Unbinned)
This tutorial picks up after you have assembled your metagenome sequences. For that you may opt to use IDBA-UD, SPAdes, or some other assembling algorithm. Since I used SPAdes I will be laying out the process I used for my SPAdes assemblies. 
</br>
</br> 

#### PROKKA
Before using `prokka`, you will need to filter your sequences for both coverage and contig length. Any contigs that are smaller than 500 bp are usually assumed to be a sequencing error, there just isn't enough support for them in your assembly. Usually the cut off for coverage, or how many times your sequence appeared in the assembly, is roughly 2. 
</br>
</br>
To filter contigs for size I downloaded the script from [tinybio](https://github.com/tinybio/filter_contigs). Remember before running this code to open a screen, in case you lose your connection with the server, and also to `cp` your assembly.fasta files to a new directory (in case something happens)! Since this script doesn't have a version number you should write down the date you accessed (downloaded) it in your notes. 
```python /your/path/filter_contigs.py 500 /your/path/assembly.fasta```
</br>
</br>
To filter my contigs for coverage I used the short python script [here](https://microsizedmind.wordpress.com/2015/03/05/removing-small-low-coverage-contigs-from-a-spades-assembly/) in the comment provided by Abraham Gihawi. This script, as written, filters for 1.5 coverage. Simply change the coverage command in the script if you want a more strenous cutoff. Remember, if using a script without a version you should write down the date you accessed it in your notes.
 
* Cite Gihawi, A., Rallapalli, G., Hurst, R., Cooper, C. S., Leggett, R. M., & Brewer, D. S. (2019). SEPATH: benchmarking the search for pathogens in human tissue whole genome sequence data leads to template pipelines. Genome biology, 20(1), 1-15.

</br>
```python /your/path/gihawi-code.py /your/path/assembly.filter500.fasta```
</br>
</br>
Finally, you can now run the prokka annotation! Using 8 threads on our server, with 6 samples, it took approximately 12 hours to get all of my results. You'll want to budget your time accordingly. Note, that in the below code, I use the `--metagenome` flag, as prokka base is not designed for use with metagenomic sequences. I've also used `--centre` and `--locustag` because the fasta headers from the SPAdes assembly do not play nice with prokka.
*Cite Seemann, T. (2014). Prokka: rapid prokaryotic genome annotation. Bioinformatics, 30(14), 2068-2069.
</br>
</br>
You'll also want to record the `prokka` version number.
```for FileName in *fasta; do prokka --centre C --locustag L --metagenome --cpus 8 --outdir your/path/output/$FileName $FileName; done```


####Prodigal
Prokka uses Prodigal as part of its assembly, but using the prodigal program in and of itself also has merit, so here I have shown Prodigal.
</br></br>
Here is the introduction, etc. to Prodigal on GitHub [Prodigal GitHub](https://github.com/hyattpd/prodigal/wiki/gene-prediction-modes)
</br></br>Anonymous mode (previously metagenomic mode) should be used for metagenomic data sets - Prodigal will use pre-calculated training sets to provide input sequences and and predict genes based on the best results. To run Prodigal in anonymous mode you add `-p anon` to your command line.

```
prodigal -i /yourpath/metagenome.fna -o coords.gbk -p anon
```

* -i = input file [stdin]
* -o = output file [stdout]
* -f = output file format (gbk genbank-like [default], gff, sqn sequin feature table, sco simple coordinate output)
* -a = protein translation of predicted genes (faa)
* -d = nucleotide sequences of predicted genes (fna)
* -p = mode (anonymous, normal, or training)

```
for FileName in *fa; do prodigal -i $FileName -o /yourpath/$FileName.gbk -p meta -a /yourpath/$FileName.faa -d /yourpath/$FileName.fna; done
```
</br>
</br> For use in the HMMs you'll want to pull the amino acid sequence fasta (.faa) for amino acid HMMs or the nucleotide (.fna) for nucleotide HMMS. 



### Finding HMM Protein Sequences
For this process I opted to use the [UniProt](https://www.uniprot.org/) database of protein sequences. <br/>
<br/>
After finding the protein sequences make sure to write down the date you accessed whatever database you choose, as well as what search terms, etc. you used for finding those sequences. These notes are very important for reproducibility. <br/>
<br/>

* Cite Boutet, E., Lieberherr, D., Tognolli, M., Schneider, M., & Bairoch, A. (2007). Uniprotkb/swiss-prot. In Plant bioinformatics (pp. 89-112). Humana Press.

### Aligning the HMM Fasta
I am now assuming that the protein sequences you downloaded are in a fasta file. Before you can proceed with building your HMM you need to align the sequences. </br>
</br>
To align the file we'll use [Clustal Omega](http://www.clustal.org/omega/README). Before running your code don't forget to open a screen session so that your work will continue even if you lose connection with the server! </br>
</br> 
``` clustalo -i /your/file/path/proteins.fasta -o ./your/file/path/proteins.sto --outfmt=st --threads=8 --force --verbose```
</br>
</br>Please note that I have opted here to use the `--verbose` flag, this works for me because I like to be able to see what the program is doing as a sanity check. If you don't care to see the calls in real-time then there is no need to use this. 
</br>
</br> I have also opted to use 8 threads on our server, as that is about the max we allocate to any one task. Your server may have different directives, please check with whomever manages it before giving too many threads to a task!
</br>
</br> Finally, before moving on make sure that you record in your bioinformatics notebook what the current version of `clustalo` on your server is. You can do that using the command `clustalo --version`.

* Cite Sievers, F., Wilm, A., Dineen, D., Gibson, T. J., Karplus, K., Li, W., ... & Thompson, J. D. (2011). Fast, scalable generation of high‐quality protein multiple sequence alignments using Clustal Omega. Molecular systems biology, 7(1), 539.

### Building the HMM
For this we will be using one of the [HMMER](http://eddylab.org/software/hmmer/Userguide.pdf) programs, `hmmbuild`. If you want more details on how this program works the Eddy Lab's User Guide is annotated very well. 
</br>
</br>
In essence, you provide a Stockholm file to `hmmbuild` and it will develop an HMM from that. So:
`hmmbuild /your/path/proteins.hmm /your/path/proteins.sto`
</br>
</br> Note that the format here is output followed by input.
</br>
</br>
Conveniently, HMMER automatically calls the version you are using in the `hmmbuild` output, all you have to do is remember to write it down! :) 

* Cite Eddy, S. R. (1995, July). Multiple alignment using hidden Markov models. In Ismb (Vol. 3, pp. 114-120).

### Test the HMM
Next, you'll want to verify that your HMM assembled correctly. For this, pull out a few of your protein sequences of interest in your original .fasta file and put them in a new file. Now, use the HMM and verify that the proteins that are called are the right ones!
```hmmsearch /your/path/proteins.hmm /your/path/protein_test.fasta > /your/path/protein.testoutput```

### Garner your Output
Now, for the easy part! After completing your annotations and building the HMM you can put the two together and make a pizza!
```hmmsearch /your/path/proteins.hmm /your/path/PROKKA_date.faa > /your/path/output/filename.output```


###LithoGenie / GeoGenie / FeGenie
You may also decide to find a pre-made HMM, sometimes you will get lucky. In the event you are interested in biogeochemical cycling you may look at the [LithoGenie](https://github.com/Arkadiy-Garber/LithoGenie) program developed in the Banfield Lab. (The program has gone by a few names which I've listed above.)

</br></br>For this example I've used the basic download and play version (GeoGenie). The program depends on prodigal and HMMER. You will want to have a copy of the HMM folder `HMM-programV.2` in your personal folder on your server. 

```
python3 /your/path/hmm-metabolisms.py -hmm_dir /yourpath/metabolic-hmms-banfield-ag/ -bin_dir /your/path/ -bin_ext fa -out /your/path/output -m hmm-meta.txt
```
```
for FileName in *fa; do python3 /your/directory/HMM-programV.2/hmm-metabolisms.py -hmm_dir /your/directory/HMM-programV.2/metabolic-hmms-banfield-ag/ -bin_dir /your/bin/directoryfaa/ -bin_ext fa -out /your/output/directory/$FileName -m /your/directory/HMM-programV.2/hmm-meta.txt; done
```


###Blast
####Formatting the Database
Another option for annotating your proteins is to make a blast database of genes of interest and blasting your genes. You can do this using the following code snippets. First, you'll need to format the blast database. 

```
makeblastdb -in /your/directory/file.faa -dbtype prot -parse_seqids -out /your/output/path
```
Then you can search the database

```
blastp -db /your/path/file.aa -query /your/path/seq.faa -evalue 1e-02 -max_target_seqs 10 -outfmt 11 -out /your/path/outputfile.asn
```

##### Parsing the Results
```
blast_formatter -archive /your/path/outputfile.asn -outfmt "10 qseqid sseqid qstart qend sstart send pident length evalue bitscore score nident mismatch gaps" -out /your/new/output/path/
```

#####Then Search for the Gene of Interest in Your Results
I opted for a simple grep search for the protein name from each annotated blast file. I then created a .csv excel file with column headers that included my sample, site, and then the genes of interest. You'll also want to record the full number of gene sequences so that you can scale the results for each sample out of total gene sequences recovered. 

```
grep -c "NP_717386" *parsedfiles
```


